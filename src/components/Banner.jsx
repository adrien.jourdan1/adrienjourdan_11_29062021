import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

export default class Banner extends React.PureComponent {
  render() {
    const { bannerText, image } = this.props;

    return (
      <Container>
        <BgImg src={image} className="logo" alt="kasa" />
        <p>{bannerText}</p>
      </Container>
    );
  }
}

Banner.propTypes = {
  bannerText: PropTypes.string,
  image: PropTypes.string.isRequired,
};

Banner.defaultProps = {
  bannerText: '',
};

const Container = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 110px;
  width: 100%;
  border-radius: 10px;
  position: relative;
  overflow: hidden;
  font-size: 24px;
  color: white;
  padding: 16px;

  @media (min-width: 768px) {
    border-radius: 25px;
    height: 165px;
    font-size: 36px;
  }

  @media (min-width: 1200px) {
    height: 220px;
    font-size: 48px;
  }

  p {
    z-index: 1;
    margin: 0;
  }
`;

const BgImg = styled.img`
  position: absolute;
  width: 100%;
  height: 100%;
  object-fit: cover;
  top: 0;
  right: 0;
  filter: brightness(70%);
`;
