import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Rating from './Rating';
import TagList from './TagList';
import Host from './Host';
import Location from './Location';
import DropDown from '../../DropDown';

export default class HousingBody extends React.PureComponent {
  render() {
    const { data } = this.props;

    return (
      <Container>
        <Info>
          <Location title={data.title} location={data.location} />
          <Host profile={data.host} />
          <TagList tags={data.tags} />
          <Rating rating={data.rating} />
        </Info>
        <Description>
          <DropDown title="Description" content={data.description} />
          <DropDown title="Équipements" content={data.equipments} />
        </Description>
      </Container>
    );
  }
}

HousingBody.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object]).isRequired,
};

const Container = styled.div`
`;

const Info = styled.div`
  margin: 30px 0;
  display: grid;
  grid-template: repeat(3, auto) / repeat(2, 1fr);
  width: 100%;
  align-items: center;
  row-gap: 20px;

  @media (min-width: 768px) {
  }

  @media (min-width: 1200px) {
    grid-template: repeat(2, auto) / repeat(2, auto);
  }
`;

const Description = styled.div`
  display: grid;
  grid-template: repeat(2, auto) / repeat(1, 1fr);
  gap: 20px;

  @media (min-width: 768px) {
    grid-template: repeat(1, auto) / repeat(2, 1fr);
  }

  @media (min-width: 1200px) {
    gap: 70px;
  }
`;
