import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

export default class Avatar extends React.PureComponent {
  render() {
    const { portrait } = this.props;

    return (
      <Container>
        <Portrait src={portrait} alt="portrait" />
      </Container>
    );
  }
}

Avatar.propTypes = {
  portrait: PropTypes.string.isRequired,
};

const Container = styled.div`
  width: 32px;
  height: 32px;
  border-radius: 50%;
  position: relative;
  overflow: hidden;

  @media (min-width: 768px) {
    width: 48px;
    height: 48px;
  }

  @media (min-width: 1200px) {
    width: 64px;
    height: 64px;
  }
`;

const Portrait = styled.img`
  position: absolute;
  width: 100%;
  height: 100%;
  object-fit: cover;
  top: 0;
  right: 0;
`;
