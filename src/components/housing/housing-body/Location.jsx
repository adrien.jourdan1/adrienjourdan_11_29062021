import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import colors from '../../../utils/style/colors';

export default class Location extends React.PureComponent {
  render() {
    const { title, location } = this.props;

    return (
      <Container>
        <Heading>{title}</Heading>
        <Subheading>{location}</Subheading>
      </Container>
    );
  }
}

Location.propTypes = {
  title: PropTypes.string.isRequired,
  location: PropTypes.string.isRequired,
};

const Container = styled.div`
  display: flex;
  flex-flow: column nowrap;
  gap: 5px;
  color: ${colors.primary};
  width: fit-content;
  grid-area: 1 / 1 / 2 / 3;

  @media (min-width: 768px) {
    grid-area: 1 / 1 / 2 / 2;
    gap: 10px;
  }

  @media (min-width: 1200px) {
  }
`;

const Heading = styled.h1`
  font-size: 18px;
  font-weight: 500;
  margin: 0;

  @media (min-width: 768px) {
    font-size: 27px;
  }

  @media (min-width: 1200px) {
    font-size: 36px;
  }
`;

const Subheading = styled.h2`
  font-size: 14px;
  font-weight: 500;
  margin: 0;

  @media (min-width: 768px) {
    font-size: 16px;
  }

  @media (min-width: 1200px) {
    font-size: 18px;
  }
`;
