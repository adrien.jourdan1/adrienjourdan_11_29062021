import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import colors from '../../../utils/style/colors';

export default class Tag extends React.PureComponent {
  render() {
    const { text } = this.props;

    return (
      <Container>
        <p>{text}</p>
      </Container>
    );
  }
}

Tag.propTypes = {
  text: PropTypes.string.isRequired,
};

const Container = styled.div`
  width: fit-content;
  min-width: 85px;
  height: 18px;
  background-color: ${colors.primary};
  border-radius: 5px;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0 15px;
  color: white;
  font-size: 10px;

  @media (min-width: 768px) {
    min-width: 100px;
    height: 22px;
    border-radius: 7.5px;
    font-size: 12px;
  }

  @media (min-width: 1200px) {
    min-width: 115px;
    height: 25px;
    border-radius: 10px;
    font-size: 14px;
  }
`;
