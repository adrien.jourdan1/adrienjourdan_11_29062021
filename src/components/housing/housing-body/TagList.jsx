import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Tag from './TagListTag';

export default class TagList extends React.PureComponent {
  render() {
    const { tags } = this.props;

    return (
      <Container>
        {tags.map((tag) => (
          <Tag key={tag} text={tag} />
        ))}
      </Container>
    );
  }
}

TagList.propTypes = {
  tags: PropTypes.oneOfType([PropTypes.array]).isRequired,
};

const Container = styled.div`
  display: flex;
  flex-flow: row wrap;
  gap: 10px;
  width: fit-content;
  grid-area: 2 / 1 / 3 / 3;

  @media (min-width: 768px) {
    grid-area: 2 / 1 / 3 / 2;
  }
`;
