import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import HostAvatar from './HostAvatar';
import colors from '../../../utils/style/colors';

export default class Host extends React.PureComponent {
  render() {
    const { profile } = this.props;
    const firstName = profile.name.split(' ').slice(0, -1).join(' ');
    const lastName = profile.name.split(' ').slice(-1).join(' ');

    return (
      <Container>
        <HostName>
          <p>{firstName}</p>
          <p>{lastName}</p>
        </HostName>
        <HostAvatar portrait={profile.picture} />
      </Container>
    );
  }
}

Host.propTypes = {
  profile: PropTypes.oneOfType([PropTypes.object]).isRequired,
};

const Container = styled.div`
  display: flex;
  flex-flow: row nowrap;
  gap: 10px;
  justify-content: center;
  align-items: center;
  width: fit-content;
  justify-self: end;
  grid-area: 3 / 2 / 4 / 3;

  @media (min-width: 768px) {
    grid-area: 1 / 2 / 2 / 3;
  }
`;

const HostName = styled.div`
display: flex;
flex-flow: column nowrap;

@media (min-width: 768px) {
  gap: 2px;
}

@media (min-width: 1200px) {
  gap: 5px;
}

  p {
    font-size: 12px;
    margin: 0;
    text-align: right;
    color: ${colors.primary};
    font-weight: 500;

    @media (min-width: 768px) {
      font-size: 16px;
    }

    @media (min-width: 1200px) {
      font-size: 18px;
    }
  }
`;
