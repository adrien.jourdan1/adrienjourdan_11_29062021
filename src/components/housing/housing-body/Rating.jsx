import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import star from '../../../assets/star.svg';
import emptyStar from '../../../assets/emptyStar.svg';

export default class Rating extends React.PureComponent {
  render() {
    const { rating } = this.props;
    const stars = [];

    for (let i = 0; i < 5; i += 1) {
      if (i < Number(rating)) {
        stars.push(<Star src={star} key={i} alt="star" />);
      } else {
        stars.push(<Star src={emptyStar} key={i} alt="empty star" />);
      }
    }

    return (
      <Container>
        {stars}
      </Container>
    );
  }
}

Rating.propTypes = {
  rating: PropTypes.string.isRequired,
};

const Container = styled.div`
  display: flex;
  flex-flow: row wrap;
  gap: 5px;
  width: fit-content;
  grid-area: 3 / 1 / 4 / 2;

  @media (min-width: 768px) {
    grid-area: 2 / 2 / 3 / 3;
    justify-self: end;
    gap: 7px;
  }
`;

const Star = styled.img`
  height: 15px;
  width: 15px;

  @media (min-width: 768px) {
    height: 22.5px;
    width: 22.5px;
  }

  @media (min-width: 1200px) {
    height: 30px;
    width: 30px;
  }
`;
