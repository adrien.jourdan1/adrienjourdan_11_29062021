import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import chevronTop from '../../assets/chevronTop.svg';

export default class Carousel extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      index: 0,
    };
  }

  render() {
    const { pictures } = this.props;
    const { index } = this.state;

    const nextImg = () => {
      this.setState((prevState) => ({
        index: (index === pictures.length - 1)
          ? 0
          : prevState.index + 1,
      }));
    };

    const previousImg = () => {
      this.setState((prevState) => ({
        index: (index === 0)
          ? pictures.length - 1
          : prevState.index - 1,
      }));
    };

    return (
      <Container>
        <BgImg src={pictures[index]} alt="" />
        <PrevBtn
          src={chevronTop}
          onClick={() => previousImg()}
          style={{ display: pictures.length > 1 ? 'block' : 'none' }}
          alt="Previous"
        />
        <NextBtn
          src={chevronTop}
          onClick={() => nextImg()}
          style={{ display: pictures.length > 1 ? 'block' : 'none' }}
          alt="Next"
        />
      </Container>
    );
  }
}

Carousel.propTypes = {
  pictures: PropTypes.oneOfType([PropTypes.array]).isRequired,
};

const Container = styled.div`
  height: 255px;
  width: 100%;
  border-radius: 10px;
  position: relative;
  overflow: hidden;

  @media (min-width: 768px) {
    height: 325px;
    border-radius: 17.5px;
  }

  @media (min-width: 1200px) {
    height: 415px;
    border-radius: 25px;
  }
`;

const BgImg = styled.img`
  position: absolute;
  width: 100%;
  height: 100%;
  object-fit: cover;
  top: 0;
  right: 0;
`;

//
// Next/Previous Buttons
const btnSize = '20px';
const btnSizeMD = '35px';
const btnSizeLG = '50px';

const NavBtn = styled.img`
  width: ${btnSize};
  position: absolute;
  z-index: 1;
  cursor: pointer;

  @media (min-width: 768px) {
    width: ${btnSizeMD};
  }

  @media (min-width: 1200px) {
    width: ${btnSizeLG};
  }
`;

const PrevBtn = styled(NavBtn)`
  transform: rotate(-90deg);
  left: 0;
  top: calc(50% - ${btnSize} / 2);

  @media (min-width: 768px) {
    top: calc(50% - ${btnSizeMD} / 2);
  }

  @media (min-width: 1200px) {
    top: calc(50% - ${btnSizeLG} / 2);
  }
`;

const NextBtn = styled(NavBtn)`
  transform: rotate(90deg);
  right: 0;
  top: calc(50% - ${btnSize} / 2);

  @media (min-width: 768px) {
    top: calc(50% - ${btnSizeMD} / 2);
  }

  @media (min-width: 1200px) {
    top: calc(50% - ${btnSizeLG} / 2);
  }
`;
