import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import colors from '../utils/style/colors';
import chevronTop from '../assets/chevronTop.svg';

export default class CollapseMenu extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    };
  }

  render() {
    const { content, title } = this.props;
    const { isOpen } = this.state;

    const toggleMenu = () => {
      this.setState((prevState) => ({
        isOpen: !prevState.isOpen,
      }));
    };

    // Dropdown menu for string prop
    const descriptionMenu = () => (
      <Container>
        <OpenBtn onClick={() => toggleMenu()}>
          {title}
          <ChevronIcon src={chevronTop} alt="chevron" style={{ transform: isOpen ? 'rotate(0)' : 'rotate(180deg)' }} />
        </OpenBtn>
        <Content style={{ display: isOpen ? 'block' : 'none' }}>
          {content}
        </Content>
      </Container>
    );

    // Dropdown menu for string[] prop
    const listMenu = () => (
      <Container>
        <OpenBtn onClick={() => toggleMenu()}>
          {title}
          <ChevronIcon src={chevronTop} alt="chevron" style={{ transform: isOpen ? 'rotate(0)' : 'rotate(180deg)' }} />
        </OpenBtn>
        <Content style={{ display: isOpen ? 'block' : 'none' }}>
          <List>
            {content.map((elt) => (
              <Item key={elt}>{elt}</Item>
            ))}
          </List>
        </Content>
      </Container>
    );

    if (typeof content === 'string') {
      return (
        descriptionMenu()
      );
    }

    return (
      listMenu()
    );
  }
}

CollapseMenu.propTypes = {
  content: PropTypes.oneOfType([PropTypes.array, PropTypes.string]).isRequired,
  title: PropTypes.string.isRequired,
};

const Container = styled.div`
  overflow: 'hidden'
`;

const OpenBtn = styled.button`
  display: flex;
  flex-flow: row nowrap;
  align-items: center;
  justify-content: space-between;
  padding: 0 10px;
  height: 30px;
  background-color: ${colors.primary};
  border-radius: 5px;
  color: white;
  font-size: 13px;
  position: relative;
  z-index: 2;
  width: 100%;
  border: none;
  cursor: pointer;

  &:hover {
    background-color: ${colors.lightenPrimary};
  }

  @media (min-width: 768px) {
    height: 41px;
    font-size: 16px;
    border-radius: 7.5px;
    padding: 0 15px;
  }

  @media (min-width: 1200px) {
    height: 52px;
    font-size: 18px;
    border-radius: 10px;
    padding: 0 20px;
  }
`;

const Content = styled.div`
  color: ${colors.primary};
  font-size: 12px;
  border-radius: 10px;
  background-color: ${colors.tertiary};
  padding: 18px 11px;
  position: relative;
  z-index: 0;
  display: none;

  @media (min-width: 768px) {
    padding: 24px 16px;
    font-size: 15px;
  }

  @media (min-width: 1200px) {
    padding: 30px 20px;
    font-size: 18px;
  }
`;

const List = styled.ul`
  list-style: none;
  display: flex;
  flex-flow: nowrap column;
  gap: 2px;
  margin: 0;
  padding: 0;

  @media (min-width: 768px) {
    gap: 5px;
  }

  @media (min-width: 1200px) {
    gap: 8px;
  }
`;

const Item = styled.li`
  font-size: 12px;

  @media (min-width: 768px) {
    font-size: 15px;
  }

  @media (min-width: 1200px) {
    font-size: 18px;
  }
`;

const ChevronIcon = styled.img`
  width: 10px;

  @media (min-width: 768px) {
    width: 18px;
  }

  @media (min-width: 1200px) {
    width: 26px;
  }
`;
