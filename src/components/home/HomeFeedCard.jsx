import React from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

export default class HomeFeedCard extends React.PureComponent {
  render() {
    const { data } = this.props;

    return (
      <Link to={`/housing/${data.id}`} style={{ textDecoration: 'none' }}>
        <Card>
          <p>{data.title}</p>
          <BgImg src={data.cover} alt={data.title} />
        </Card>
      </Link>
    );
  }
}

HomeFeedCard.propTypes = {
  data: PropTypes.oneOfType([PropTypes.object]).isRequired,
};

const Card = styled.div`
  padding: 20px;
  border-radius: 10px;
  height: 255px;
  display: flex;
  align-items: flex-end;
  position: relative;
  overflow: hidden;

  @media (min-width: 768px) {
    height: 295px;
  }

  @media (min-width: 1200px) {
    height: 340px;
  }

  p {
    font-size: 18px;
    color: white;
    max-width: 65%;
    font-weight: 600;
    z-index: 1;
  }
`;

const BgImg = styled.img`
  position: absolute;
  width: 100%;
  height: 100%;
  object-fit: cover;
  top: 0;
  right: 0;
`;
