import React from 'react';
import styled from 'styled-components';
import data from '../../utils/fixture/data';
import HomeFeedCard from './HomeFeedCard';

export default class HomeFeed extends React.PureComponent {
  render() {
    return (
      <Feed>
        {data.map((location) => (
          <HomeFeedCard key={location.id} data={location} />
        ))}
      </Feed>
    );
  }
}

// Define Grid cols/rows
const colNumberLg = 3;
const colNumberMd = 2;
const colNumber = 1;
const rowsNumberLg = Math.ceil(data.length / colNumberLg);
const rowsNumberMd = Math.ceil(data.length / colNumberMd);
const rowsNumber = data.length;

const Feed = styled.div`
  display: grid;
  grid-template: repeat(${rowsNumber}, auto) / repeat(${colNumber}, 1fr);
  grid-gap: 20px;
  width: 100%;
  margin-top: 25px;

  @media (min-width: 768px) {
    padding: 30px;
    border-radius: 25px;
    background-color: #F6F6F6;
    grid-template: repeat(${rowsNumberMd}, auto) / repeat(${colNumberMd}, 1fr);
    grid-gap: 30px;
    margin-top: 35px;
  }

  @media (min-width: 1200px) {
    padding: 40px;
    grid-template: repeat(${rowsNumberLg}, auto) / repeat(${colNumberLg}, 1fr);
    grid-gap: 40px;
    margin-top: 45px;
  }
`;
