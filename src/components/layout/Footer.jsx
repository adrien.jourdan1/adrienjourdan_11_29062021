import React from 'react';
import styled from 'styled-components';
import logo from '../../assets/logoWhite.png';

export default class Footer extends React.PureComponent {
  render() {
    return (
      <Container>
        <Logo src={logo} className="logo" alt="kasa" />
        <p>© Kasa, All rights reserved</p>
      </Container>
    );
  }
}

const Container = styled.footer`
  display: flex;
  flex-flow: column nowrap;
  justify-content: space-around;
  align-items: center;
  background-color: black;
  height: 150px;
  font-size: 12px;

  /* full width trick */
  /* https://codepen.io/tigt/post/bust-elements-out-of-containers-with-one-line-of-css */
  margin: 0 calc(50% - 50vw);
  margin-top: auto;

  @media (min-width: 768px) {
    font-size: 18px;
    height: 175px;
  }

  @media (min-width: 1200px) {
    font-size: 24px;
    height: 200px;
  }

  p {
    color: white;
    margin: 0;
  }
`;

const Logo = styled.img`
  margin-top: 10px;
  height: 40px;

  @media (min-width: 768px) {
    margin-top: 25px;
  }

  @media (min-width: 1200px) {
    margin-top: 40px;
  }
`;
