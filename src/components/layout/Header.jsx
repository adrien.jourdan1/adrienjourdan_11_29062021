import { Link } from 'react-router-dom';
import React from 'react';
import styled from 'styled-components';
import logo from '../../assets/logo.png';
import colors from '../../utils/style/colors';

export default class Header extends React.PureComponent {
  render() {
    return (
      <Container>
        <Logo src={logo} className="logo" alt="kasa" />
        <Nav>
          <Link to="/">Accueil</Link>
          <Link to="/about">À Propos</Link>
        </Nav>
      </Container>
    );
  }
}

const Container = styled.header`
  margin-bottom: 30px;
  display: flex;
  flex-flow: row wrap;
  justify-content: space-between;
  align-items: center;

  @media (min-width: 768px) {
    margin-bottom: 45px;
  }

  @media (min-width: 1200px) {
    margin-bottom: 70px;
  }
`;

const Logo = styled.img`
  height: 47px;

  @media (min-width: 768px) {
    height: 58px;
  }

  @media (min-width: 1200px) {
    height: 68px;
  }
`;

const Nav = styled.nav`
  display: flex;
  flex-flow: row wrap;
  gap: 10px;

  @media (min-width: 768px) {
    gap: 25px;
  }

  @media (min-width: 1200px) {
    gap: 50px;
  }

  a {
    color: ${colors.primary};
    font-size: 14px;
    font-weight: 500;
    text-decoration: none;

    @media (min-width: 768px) {
      font-size: 18px;
    }

    @media (min-width: 1200px) {
      font-size: 24px;
    }

    &:hover {
      text-decoration: underline;
      color: ${colors.lightenPrimary};
    }
  }
`;
