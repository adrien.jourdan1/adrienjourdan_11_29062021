import React from 'react';
import styled from 'styled-components';
import Banner from '../../components/Banner';
import BannerImg from '../../assets/banner2.jpg';
import DropDown from '../../components/DropDown';
import about from '../../utils/fixture/about';

export default class About extends React.PureComponent {
  render() {
    return (
      <Container>
        <Banner image={BannerImg} />
        <ConventionList>
          <DropDown title="Fiabilité" content={about.reliability} />
          <DropDown title="Respect" content={about.respect} />
          <DropDown title="Service" content={about.service} />
          <DropDown title="Sécurité" content={about.security} />
        </ConventionList>
      </Container>
    );
  }
}

const Container = styled.section`
  display: flex;
  flex-flow: column nowrap;
  align-items: center;
  margin-bottom: 60px;

  @media (min-width: 768px) {
    margin-bottom: 90px;
  }

  @media (min-width: 1200px) {
    margin-bottom: 120px;
  }
`;

const ConventionList = styled.div`
  margin-top: 20px;
  display: flex;
  flex-flow: column nowrap;
  gap: 20px;
  max-width: 1025px;
  width: 100%;

  @media (min-width: 768px) {
    gap: 26px;
    margin-top: 26px;
  }

  @media (min-width: 1200px) {
    gap: 32px;
    margin-top: 32px;
  }
`;
