import React from 'react';
import styled from 'styled-components';
import data from '../../utils/fixture/data';
import Carousel from '../../components/housing/carousel';
import HousingBody from '../../components/housing/housing-body';
import Error from '../Error';

export default class Housing extends React.PureComponent {
  render() {
    // eslint-disable-next-line react/prop-types
    const { housingId } = this.props;
    const currentLocation = data.find((location) => location.id === housingId);

    if (!currentLocation) {
      return (
        <Error />
      );
    }

    return (
      <Container>
        <Carousel pictures={currentLocation.pictures} />
        <HousingBody data={currentLocation} />
      </Container>
    );
  }
}

const Container = styled.section`
  margin-bottom: 60px;

  @media (min-width: 768px) {
    margin-bottom: 90px;
  }

  @media (min-width: 1200px) {
    margin-bottom: 120px;
  }
`;
