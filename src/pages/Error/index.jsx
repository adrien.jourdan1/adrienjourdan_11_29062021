import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import colors from '../../utils/style/colors';

export default class Error extends React.PureComponent {
  render() {
    return (
      <Container>
        <Heading>404</Heading>
        <Subheading>Oups ! La page que vous demandez n&apos;existe pas.</Subheading>
        <Link to="/">Retourner sur la page d&apos;accueil</Link>
      </Container>
    );
  }
}

const Container = styled.section`
  display: flex;
  flex-flow: column nowrap;
  justify-content: center;
  align-items: center;
  color: ${colors.primary};
  margin: auto;

  a {
    font-size: 14px;
    color: ${colors.primary};
    margin: 0;
    margin-bottom: 60px;

    @media (min-width: 768px) {
      margin-bottom: 90px;
      font-size: 16px;
    }

    @media (min-width: 1200px) {
      margin-bottom: 120px;
      font-size: 18px;
    }
  }
`;

const Heading = styled.h1`
  font-size: 100px;
  font-weight: 700;
  margin: 0 0 11px 0;

  @media (min-width: 768px) {
    font-size: 165px;
    margin: 0 0 38px 0;
  }

  @media (min-width: 1200px) {
    font-size: 225px;
    margin: 0 0 65px 0;
  }
`;

const Subheading = styled.h2`
  font-size: 18px;
  font-weight: 500;
  margin: 0 0 133px 0;
  text-align: center;

  @media (min-width: 768px) {
    font-size: 27px;
    margin: 0 0 160px 0;
  }

  @media (min-width: 1200px) {
    font-size: 36px;
    margin: 0 0 182px 0;
  }
`;
