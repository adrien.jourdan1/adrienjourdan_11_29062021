import React from 'react';
import styled from 'styled-components';
import Banner from '../../components/Banner';
import HomeFeed from '../../components/home/HomeFeed';
import BannerImg from '../../assets/banner1.jpg';

export default class Home extends React.PureComponent {
  render() {
    return (
      <Container>
        <Banner
          bannerText="Chez vous, partout et ailleurs"
          image={BannerImg}
        />
        <HomeFeed />
      </Container>
    );
  }
}

const Container = styled.section`
  margin-bottom: 60px;

  @media (min-width: 768px) {
    margin-bottom: 90px;
  }

  @media (min-width: 1200px) {
    margin-bottom: 120px;
  }
`;
