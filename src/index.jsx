/* eslint-disable react/jsx-props-no-spreading */
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Home from './pages/Home/index';
import Header from './components/layout/Header';
import Housing from './pages/Housing/index';
import Error from './pages/Error/index';
import About from './pages/About/index';
import Footer from './components/layout/Footer';
import './utils/style/global.css';

ReactDOM.render(
  <React.StrictMode>
    <Router>
      <Header />
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route
          path="/housing/:housingId"
          render={(props) => <Housing {...props.match.params} />}
        />
        <Route path="/about">
          <About />
        </Route>
        <Route>
          <Error />
        </Route>
      </Switch>
      <Footer />
    </Router>
  </React.StrictMode>,
  document.getElementById('root'),
);
