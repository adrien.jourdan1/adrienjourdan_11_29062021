export default {
  primary: '#FF6060',
  lightenPrimary: '#ff6f6f',
  secondary: '#E3E3E3',
  tertiary: '#F7F7F7',
};
